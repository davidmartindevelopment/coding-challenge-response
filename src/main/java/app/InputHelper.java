package app;

import static java.lang.Integer.max;
import static java.lang.Integer.min;

public class InputHelper {

    public InputHelper() {}

    static String convertInput(String input) {
        int inputInt = restrictRange(Integer.parseInt(input));
        String inputBinaryString = Integer.toBinaryString(inputInt);

        return fillZeroes(inputBinaryString);
    }

    // Restrict the range of integers to 0 - 255. May be desirable to return an error rather than use a min/max function
    static int restrictRange(int input) {
        return max(min(input,255), 0);
    }

    // Add leading zeroes to the binary string
    static String fillZeroes(String binString) {
        // Magic number here is not ideal.
        int desiredLength = 8;
        String zeroes = "0";
        String leadingZeroes = zeroes.repeat(desiredLength - binString.length());

        return leadingZeroes.concat(binString);
    }

}
