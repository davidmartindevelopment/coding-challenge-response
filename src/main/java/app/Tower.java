package app;

// This class contains the core logic of the tower.
public class Tower {
    // Set max height of tower.
    private int maxTowerHeight = 8;
    // This could be a local variable within resetTower
    private int rowLength = 8;
    private int nextRow = 0;
    private String[] towerState;

    public Tower() {
        towerState = new String[maxTowerHeight];
        resetTower();
    }

    // Reset/init our tower representation.
    public void resetTower() {
        String emptyRow = "0".repeat(rowLength);
        nextRow = 0;
        for (int i = 0; i < maxTowerHeight; i++ ){
            towerState[i] = emptyRow;
        }
    }

    // Attempt to add a row to our tower representation.
    // If past max height reset then add
    // If invalid reset
    public String addRow(String row) {
        // If past our maximum height first reset state.
        if (nextRow > (maxTowerHeight - 1)) {
            resetTower();
        }

        // If the row is valid add it (currently at 'top' of tower)
        if (validRow(row)) {
            towerState[nextRow] = row;
            nextRow += 1;
        }
        // Else reset our state and ignore the input.
        else
        {
            resetTower();
        }
        return currentState();
    }

    // Return a nice printable string of our state.
    public String currentState() {
        String res = "";
        for (String row: towerState) {
            // Add a new line to each row
            String newline_row = row.concat("\n");

            // Reverse the print order of the rows so they build 'up'
            res = newline_row.concat(res);
        }
        return res;
    }

    // Most important part of the logic goes here. Does nothing yet.
    public boolean validRow(String row) {
        // If first row then res is true. Otherwise check.
        boolean res = true;

        if (nextRow > 0) {
            char[] lastRowChars = towerState[nextRow-1].toCharArray();
            char[] rowChars = row.toCharArray();

            for (int i = 0; i < rowChars.length; i++) {
                char top = rowChars[i];
                char bottom = lastRowChars[i];

                // Only false if top is 1 but bottom is 0
                if (Character.getNumericValue(top) == 1 && Character.getNumericValue(bottom) == 0) {
                    res = false;
                }
            }
        }

        return res;
    }
}
