package app;

import java.util.Random;
import java.lang.Math;

import static java.lang.Integer.max;
import static java.lang.Integer.min;
import static spark.Spark.*;

public class Application {

    public static void main(String[] args) {

        Adder adder = new Adder();
        Tower tower = new Tower();

        port(4567);

        get("/ping", (req, res) -> {
           return "Hello, World!\n";
        });

//      Note :input is not an optional parameter.
        get("/calc/:input", (req, res) -> {
//            int inputNumber = new Random().nextInt(10);
//            return adder.add(inputNumber);
            String inputString = req.params(":input");

            return tower.addRow(InputHelper.convertInput(inputString));
        });

    }

}
