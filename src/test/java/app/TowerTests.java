package app;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TowerTests {
    private Tower tower = new Tower();
    private String ones_string = "11111111";
    private String zeroes_string = "00000000";

    @Before
    public void beforeEach() {
        tower.resetTower();
    }

    @Test
    public void testValidRowOne() {
        assertTrue(tower.validRow(ones_string));
        tower.addRow(ones_string);
        assertTrue(tower.validRow(ones_string));
    }

    @Test
    public void testValidRowTwo() {
        assertTrue(tower.validRow(zeroes_string));
        tower.addRow(zeroes_string);
        assertFalse(tower.validRow(ones_string));
    }

}
